import math
from matplotlib import pyplot as plt
import numpy as np

RANGE_POINT = 0.05 # [m] これ以上既存の点と離れていたら新規としてマップに追加

class map_list:
    def __init__(self, type, pos, br_id, br_rad, flag):
        self.type = type
        self.id = id  # id
        self.pos = pos  # [x,y]
        self.br_id = br_id  # id_list []
        self.br_rad = br_rad  # rad_list []
        self.flag = flag

    def add_map(self, pos, br_id, br_rad):
        self.pos.append(pos)
        self.br_id.append(br_id)
        self.br_rad.append(br_rad)
        #self.id.append(len(self.id))

    def calc_dist(self, point:[]):
        r_min = -1
        print(self.pos)
        for n in range(len(self.pos)):
            a = np.array([point[0] - self.pos[n][0], point[1] - self.pos[n][1]])
            r = np.linalg.norm(a)
            if r_min == -1:
                r_min = r
            elif r < r_min:
                r_min = r
        return r_min

    def reg(self, pts:[[]]):
        if self.flag == 1:  # 初回処理
            self.flag = 0
            for n in range(len(pts)):
                if self.type == "moon":
                    br_vecs = find_vec(pts[n], map_circle)  # 現在いる枝をid 0とする
                    br_ids = []
                    for m in range(len(br_vecs)):
                        br_ids.append(m)
                    self.add_map([pts[n][0], pts[n][1]], br_ids, br_vecs)
                else: # circle
                    self.add_map([pts[n][0], pts[n][1]], 0, 0)

        else: # 2回目以降
            for n in range(len(pts)):
                r = self.calc_dist(pts[n])

                if r > RANGE_POINT: # 新規点
                    if self.type == "moon":
                        #書き直し
                        br_ids = []
                        br_vecs = find_vec(pts[n], map_circle)
                        x = pts[n][0] - machine_x
                        y = pts[n][1] - machine_y
                        rad = math.atan2(y, x)  # 発見した月と現在のマシンがなす角度(ワールド座標基準)
                        minimum = 100
                        for m in range(len(br_vecs)): # 発見した枝のうち、角度が一番近いものは今いる枝とみなす
                            tmp = math.fabs(br_vecs[m] - rad)
                            if tmp < minimum:
                                minimum = tmp
                                num = m
                        del br_vecs[num] # 現在いる枝を発見した枝リストから削除
                        tail = branch_list[len(branch_list)-1] # 末尾要素の取得
                        for m in range(0, len(br_vecs)-len(self.br_id), 1): # 0から未知個数分だけ
                            branch_list.apprnd(tail + m)
                            br_ids.apprnd(tail + m)
                        self.add_map([pts[n][0], pts[n][1]], br_ids, br_vecs)
                    else: # サークルの場合、枝の概念なし
                        self.add_map([pts[n][0], pts[n][1]], 0, 0)

                else: # 既知点
                    if self.type == "moon":
                        # 枝の追加確認 もし数が多ければ...
                        br_vecs = find_vec(pts[n], map_circle)
                        if len(br_vecs) > len(self.br_id):
                            tail = branch_list[len(branch_list) - 1] # 末尾idの取得
                            for m in range(0, len(br_vecs) - len(self.br_id), 1):  # 未知個数分だけ
                                branch_list.apprnd(tail + m)







map_moon = map_list("moon", [], [], [], 1)
map_circle = map_list("circle", [], [], [], 1)
branch_list = [0]
branch_current = 0
machine_x = 0
machine_y = 0


def get_map(moon, cir):
    if len(cir) != 0:
        map_list.reg(map_circle, cir)

    if len(moon) != 0:
        map_list.reg(map_moon, moon)

    x = [0,0]
    y = [0,0]
    x.clear()
    y.clear()
    for n in range(len(map_circle.pos)):
        x.append(map_circle.pos[n][0])
        y.append(map_circle.pos[n][1])
    plt.scatter(x, y)
    plt.show()

    return


def circle_calc(machine_x, machine_y, yaw, point_cam):
    # マシンからの相対座標をワールド座標へ変換
    # point_cam  が [[x,y], [x,y], ...] ならば
    point_w = []
    yaw = -yaw
    for n in point_cam:
        x = point_cam[n][0]
        y = point_cam[n][1]
        u = math.cos(yaw) * x + math.sin(yaw) * y
        v = -math.sin(yaw) * x + math.cos(yaw) * y
        point_w[n][0] = machine_x + u
        point_w[n][1] = machine_y + v
    return point_w # ワールド座標でみたカメラのマーカー座標




def find_vec(moon, cir_l:map_list):
    # moon = [x,y]
    # cir_l = map_list型のcircle_data
    cir_near = []
    cir_near = cir_l.pos.copy()

    for n in range(cir_near):
        x = cir_l.pos[n][0] - moon[0]  # X
        y = cir_l.pos[n][1] - moon[1]  # Y

        r = math.sqrt(pow(x, 2) + pow(y, 2))
        th = math.atan2(y, x)
        cir_near[n].append(r)
        cir_near[n].append(th)
    # これでcir_lはあるmoonを基準に[x,y,r,theta]

    # 近い順に6個残す

    cir_near = sorted(cir_near, key=lambda x: x[2])
    if len(cir_near) > 6 :
        del cir_near[6:len(cir_l.pos)]
    print ("near")
    print (cir_near)

    x = [cir_near[0][0]]
    y = [cir_near[0][1]]
    for n in range(1, 5):
        x.append(cir_near[n][0])
        y.append(cir_near[n][1])
    plt.scatter(x, y)
    plt.show()

    # 角度順
    cir_near = sorted(cir_l, key=lambda x: x[3])
    print ("rad")
    print (cir_near)

    cir_br = [cir_near[0][3]]
    temp = cir_near[0][3]
    for n in range(5):
        # 角度差が20deg
        if math.fabs(temp - cir_near[n+1][3]) > math.radians(20):
            cir_br.append(cir_near[n+1][3])
            temp = cir_near[n+1][3]
    return cir_br  # radのリスト




'''
print(map.pos)
map_list.add_map(map, [3,3], [5], [7,7,7])
print(map.pos)
print(map.br_rad)

bbb = [55,55]
map_list.cir_reg(map, bbb)
print(map.pos)
'''