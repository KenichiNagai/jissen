
import math
import numpy as np

CIRCLE_RANGE = 0.03

# 原点xyから特定のXYへの極座標(r, Θ)を求める
def getxy_RD(x, y, X, Y):
    _x, _y = (X-x), (Y-y)
    r = math.sqrt(_x**2+_y**2)
    rad = math.atan2(_y, _x)
    degree = math.degrees(rad)
    # print(r, degree)
    return [r, degree]

# 極座標(r, Θ)から直交座標(x, y)を求める
def getXY(pole):
    # pole = [r, degree]
    # 度をラジアンに変換
    r = pole[0]
    degree = pole[1]

    rad = math.radians(degree)
    x = r * math.cos(rad)
    y = r * math.sin(rad)
    # print(x, y)
    return [x, y]

# ベクトル群とベクトルを比較し、大きさの差が最も小さいベクトル、インデクスを返す
def calc_min_error_vec(vec: [], vectors: [[]]):
    gap_min_vec = [1000, 1000]
    gap_vec = []
    np.array(gap_min_vec)
    np.array(gap_vec)
    np.array(vec)
    for index in range(len(vectors)):
        np.array(vectors[index])
        gap_vec = vectors - vec
        gap_vec_norm = np.linalg.norm(gap_vec)
        gap_min_vec_norm = np.linalg.norm(gap_min_vec)
        if gap_vec_norm < gap_min_vec_norm:
            gap_min_vec = gap_vec
            index_min = index
    return gap_min_vec


circles_past = [[]]

def pic_pointmaching(circles: [[]]):
    # circles = [[x,y],...] 機体中心からの相対座標

    global circles_past
    candidates = [[]]
    cand_vec = []
    vectors = []

    # サークル位置[x,y]を機体座標(過去)から極座標[r, deg]化
    circles_past_pole = [[]]

    candidates_th0 = []  # appendで[[x,y]]になる
    candidates_r0 = []  # appendで[[x,y]]になる
    # 移動先の候補を前後方向-1~+1cm, 角度方向-5~+5[deg]で用意する

    if len(circles_past[0]) != 0:
        # print('AA')
        circles_past_pole.clear()
        candidates.clear()

        for y in range(-10, 11, 1):  # start, stop, step 単位m
            for n in range(len(circles_past)):
                candidates_th0.append([0, y/1000])

        for theta in range(-5, 6, 1):

            # print(circles_past)

            for n in range(len(circles_past)):
                # サークル座標(過去)を極座標変換
                circles_past_pole.append(getxy_RD(0, 0, circles_past[n][0], circles_past[n][1]))
                # theta成分のみを加算
                circles_past_pole[n][1] += theta
                # XY座標に戻して、候補とする
                candidates_r0.append(getXY(circles_past_pole[n]))

        np.array(cand_vec)
        for y in range(len(candidates_th0)):
            candidates.append([])
            for theta in range(len(candidates_r0)):
                # 移動先候補のベクトル
                cand_vec = np.array(candidates_th0[y]) + np.array(candidates_r0[theta])
                # print(y, theta)
                # print(cand_vec)
                candidates[y].append(cand_vec)

        vectors.clear()
        gap_ave_min = 100000

        for y in range(len(candidates)):
            for theta in range(len(candidates[y])):
                # 候補リストから1つを選んだ

                for c in range(len(circles)):
                    np.array(circles[c])
                    # 現在のサークルから1つ選ぶ

                    vec1 = []
                    np.array(vec1)
                    vec1 = calc_min_error_vec(circles[c], candidates[y][theta])
                    vec1_norm = np.linalg.norm(vec1)

                    # 誤差が大きいサークルは新規円として無視
                    if vec1_norm < CIRCLE_RANGE:
                        vectors.append(vec1)
                # 候補リストマップに対応するvectorsが完成した

                gap_ave = 0

                # 誤差平均を求める
                for n in range(len(vectors)):
                    gap = calc_min_error_vec(vectors[n], candidates[y][theta])
                    gap_ave += np.linalg.norm(gap)
                    gap_ave /= len(vectors)


                # print(gap_ave)
                # print(gap_ave_min)
                if gap_ave < gap_ave_min:
                    gap_ave_min = gap_ave
                    y_index = y
                    theta_index = theta

        y_gap = (y_index -10) /1000
        theta_gap = theta_index -5  # [deg]

        circles_past = circles

        return y_gap, theta_gap # m, deg
    else:
        circles_past = circles
        return 0, 0














    # 現在の座標と、誤差の総和が少ないものはどれか


    # それをロボットのx, y, thetaの変位として採用する。


pic_pointmaching([[0,0],[0.01,0.01]])
print(pic_pointmaching([[0.01,0.01],[0.03,0.03]]))

# 計算量オーダーが大きすぎて1回の計算に25秒かかるのでゴミ