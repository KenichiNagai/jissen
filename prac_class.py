import math
import numpy as np

class moon_data:
    def __init__(self, pos, br_id, br_rad):
        self.pos = pos # [x,y]
        self.br_id = br_id # id_list
        self.br_rad = br_rad # rad_list

moon_list = []

ap = moon_data([10, 10], [0, 2], [1.2, 2.4])
moon_list.append(ap)

ap = moon_data([20, 25], [1, 2], [0.5, 0.8])
moon_list.append(ap)

print(moon_list[0].br_id[1])


class map_list:
    def __init__(self, pos, br_id, br_rad):
        self.id = id  # id
        self.pos = pos  # [x,y]
        self.br_id = br_id  # id_list []
        self.br_rad = br_rad  # rad_list []

    def add_map(self, pos, br_id, br_rad):
        self.pos.append(pos)
        self.br_id.append(br_id)
        self.br_rad.append(br_rad)

    def calc_dist(self, point:[int, int]):
        r_min = -1
        for n in range(len(self.pos)):
            a = np.array([point[0] - self.pos[n][0], point[1] - self.pos[n][1]])
            r = np.linalg.norm(a)
            if r_min == -1:
                r_min = r
            elif r < r_min:
                r_min = r
        return r_min

    def cir_reg(self, pt:[int, int]):
        r = self.calc_dist(pt)
        if r > 40:
            self.add_map(pt, 0, 0)

    def moon_reg(self, pt:[int, int], circle):
        r = self.calc_dist(pt)
        if r > 40:
            self.add_map(self, pt, 0, 0)



map = map_list([[1,1]], [[1,2]], [[3,4]])
print(map.pos)
map_list.add_map(map, [3,3], [5], [7,7,7])
print(map.pos)
print(map.br_rad)

bbb = [55,55]
map_list.cir_reg(map, bbb)
print(map.pos)