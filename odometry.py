import math
import random
import time
from numpy import array

# 現在座標
pos_x = 0
pos_y = 0
theta = 0

# パラメータ
D = 10 # mm タイヤ直径
W = 100 # mm タイヤ間

deg_r = 0
deg_l = 0

t = 0.1 # sec

while True:
    '''
	result = BrickPiUpdateValues()
	Encoder_A_2 = BrickPi.Encoder[PORT_A]
	Encoder_B_2 = BrickPi.Encoder[PORT_B]
	Encoder_C_2 = BrickPi.Encoder[PORT_C]
	Encoder_D_2 = BrickPi.Encoder[PORT_D]
    '''
    # 疑似環境

    deg_r += 2
    deg_l += 1
    rad_r = math.radians(deg_r)
    rad_l = math.radians(deg_l)

    # タイヤ回転角速度(rad/t)
    rad_r_past = deg_r
    rad_l_past = deg_l
    omega_r = (rad_r - rad_r_past)
    omega_l = (rad_l - rad_l_past)

    # タイヤ進行速度 mm/t
    u_r = D/2 * omega_r
    u_l = D/2 * omega_l

    omega_m = (u_r - u_l) /2/W
    v_m = (u_r + u_l) /2


    theta = D /2 /W *(rad_r - rad_l)
    v_x = D / 4 * (u_r + u_l) * -math.sin(theta)
    v_y = D / 4 * (u_r + u_l) * math.cos(theta)
    pos_x += v_x * t
    pos_y += v_y * t

    print (pos_x, pos_y, theta)
    time.sleep(0.2)
