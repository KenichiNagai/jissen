import cv2
import numpy as np
import time
from wsgiref.simple_server import make_server
from urllib.parse import parse_qs
from collections import deque
from threading import Thread

import math
import random
from R_simulate import Computer_py3_garyo

# 月と●の重心座標がXY




def find_vec(moon, cir_l):
    for n in range(CIR_NUM):
        cir_l[n][0] -= moon[0]  # X
        cir_l[n][1] -= moon[1]  # Y

        r = math.sqrt(pow(cir_l[n][0], 2) + pow(cir_l[n][0], 2))
        th = math.atan2(cir_l[n][1], cir_l[n][0])
        cir_l[n].append(r)
        cir_l[n].append(th)
    # これでcir_lはあるmoonを基準に[x,y,r,theta]

    # 近い順に6個残す
    cir_near = sorted(cir_l, key=lambda x: x[2])
    del cir_near[6:CIR_NUM]
    print ("near")
    print (cir_near)

    x = [cir_near[0][0]]
    y = [cir_near[0][1]]
    for n in range(1, 5):
        x.append(cir_near[n][0])
        y.append(cir_near[n][1])
    plt.scatter(x, y)
    plt.show()

    # 角度順
    cir_near = sorted(cir_l, key=lambda x: x[3])
    print ("rad")
    print (cir_near)

    cir_br = [cir_near[0][3]]
    temp = cir_near[0][3]
    for n in range(5):
        # 角度差が20deg
        if math.fabs(temp - cir_near[n+1][3]) > math.radians(20):
            cir_br.append(cir_near[n+1][3])
            temp = cir_near[n+1][3]
    return cir_br  # radのリスト


from matplotlib import pyplot as plt

moon_list = []
cir_list = []
ap = []

CIR_NUM = 10  # てきとう



class map_list:
    def __init__(self, pos, br_id, br_rad):
        self.id = id  # id
        self.pos = pos  # [x,y]
        self.br_id = br_id  # id_list []
        self.br_rad = br_rad  # rad_list []

    def add_map(self, pos, br_id, br_rad):
        self.pos.append(pos)
        self.br_id.append(br_id)
        self.br_rad.append(br_rad)

    def calc_dist(self, point:[int, int]):
        r_min = -1
        ref = [0,0]
        for n in range(len(self.pos)):
            ref[0] = point[0] - self.pos[n][0]
            ref[1] = point[1] - self.pos[n][1]
            r = math.sqrt(ref[0]**2 + ref[1]**2)
            if r_min == -1:
                r_min = r
            elif r < r_min:
                r_min = r
        return r_min

    def cir_reg(self, pt:[int, int]):
        r = self.calc_dist(pt)
        if r > 40:
            self.add_map(pt, 0, 0)

    def moon_reg(self, pt:[int, int], circle):
        r = self.calc_dist(pt)
        if r > 40:
            self.add_map(self, pt, 0, 0)


map_moon = map_list([[]], [[]], [[]])
map_circle = map_list([],[],[])


print(map.pos)
map_list.add_map(map, [3,3], [5], [7,7,7])
print(map.pos)
print(map.br_rad)

bbb = [55,55]
map_list.cir_reg(map, bbb)
print(map.pos)