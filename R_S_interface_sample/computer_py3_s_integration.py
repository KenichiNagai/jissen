#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
ネットワークを介してデータをやり取りする場合のクライアント側のサンプルプログラム
位置と候補ベクトルをs_interface.pyに送り、選択された方向を受け取ります。

s_interface.pyとの通信部分を各自のcomputer.pyに移植するとデータの受け渡しができるはずです（未検証）

"""

import numpy as np
import urllib.request, urllib.error, urllib.parse, requests
import cv2
import random
import time
import socket


def send_cmd_direction(vdirect):
    """ 走行方向指令(複素数で指定) """
    address = 'http://%s:%s' % (ip, port)
    vdirect = '{} {}'.format(int(vdirect.real), int(vdirect.imag))
    request = '/?direction=%s' % vdirect
    # print(address + request)
    requests.post(address + request)


# ------ Main --------------

# HTTPサーバのポート設定　============
ip, port = '127.0.0.1', 8888

# 画像取得　============
stream = urllib.request.urlopen('http://%s:%s/?action=stream' % (ip, port))
bytes = bytes(b'')
count = 0

# 指令の指定　============
pi_commands = ['forward', 'left', 'right', 'backward']
pi_commands_vec = [complex(1, 0), complex(0, 1), complex(0, -1), complex(-1, 0)]
Timeout = 2  # second
t0 = 0

# main loop　============
# ソケット通信するための設定
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    # サーバを指定
    s.connect(('127.0.0.1', 50000))

    while 1:
        bytes += stream.read(1024)
        a = bytes.find(b'\xff\xd8')
        b = bytes.find(b'\xff\xd9')
        # take video from stream
        if not (a != -1 and b != -1):
            continue

        # Simulation is kept as simple as possible, it's not a real
        # mjpeg stream but a single jpg image.
        # The below line only needed when receiving img from simulation
        stream = urllib.request.urlopen('http://%s:%s/?action=stream' % (ip, port))

        jpg = bytes[a:b + 2]
        bytes = bytes[b + 2:]

        img = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
        cv2.imshow('received', img)
        # ここに画像処理を追加

        key = cv2.waitKey(80)

        if key == 27:  # break when press ESC
            break

        if time.time() - t0 > Timeout:
            # 現在はランダムに走行指令を生成　<= 画像処理によって指令を決定するプログラミングを追加
            # cmd_vdirect = random.choice(pi_commands_vec)

            # -------ここからSと通信を行う-------
            # 送るデータを作成
            position = complex(0, 0)  # 各自で書き換える
            send_data = "{} {} ".format(int(position.real), int(position.imag))
            for x in pi_commands_vec:  # pi_commands_vecを変える
                send_data += "{} {} ".format(int(x.real), int(x.imag))
            # サーバにメッセージを送る
            s.sendall(send_data.encode())

            # Sから移動方向を受け取るまでループ
            while True:
                # データを受け取る
                data = s.recv(1024)
                if data:
                    break
                time.sleep(0.5)  # 無いとフリーズする

            #  受け取ったデータを文字列に変換
            data = data.decode()
            # 文字をcomplex型に変換
            pre = [int(x) for x in data.split()]
            cmd_vdirect = complex(pre[0], pre[1])
            # -------ここまでSと通信を行う-------

            Timeout = random.randint(1, 5)
            t0 = time.time()
            print("vdirect:", cmd_vdirect)  # 複素数の走行指令（方向）

        send_cmd_direction(cmd_vdirect)
