"""
s_interface.py

ネットワークを介してデータをやり取りする場合のサーバ側のサンプルプログラム
位置と候補ベクトルをcomputer_py3_s_integration.pyから受け取り、
候補の中からランダムに選択する実装です。

このプログラムをdfs_hint_py3に移植するとデータの受け渡しができるはずです（未検証）

"""
import socket
import time

import random

# AF = IPv4 という意味
# TCP/IP の場合は、SOCK_STREAM を使う
# ソケット通信するための設定
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

    print('server start')

    # IPアドレスとポートを指定
    s.bind(('127.0.0.1', 50000))
    # 1 接続
    s.listen(1)
    # connection するまで待つ

    while True:
        # 誰かがアクセスしてきたら、コネクションとアドレスを入れる
        conn, addr = s.accept()
        with conn:
            while True:
                time.sleep(0.5)  # 無いとフリーズする

                # データを受け取る
                data = conn.recv(1024)
                if not data:
                    break

                # 受け取ったデータをstring型に変換
                data = data.decode()
                # 受け取ったデータを位置と候補ベクトルに分ける
                pre = [int(x) for x in data.split()]
                raw = [complex(*pre[i:i + 2]) for i in range(0, len(pre), 2)]
                position, candidate = raw[0], raw[1:]

                # 候補をランダムに選ぶ
                choice = random.choice(candidate)

                print("-----------------------")
                print("candidate")
                for iloop, x in enumerate( candidate ):
                    print("{}: {}".format(iloop, x))
                print("")
                print("choice")
                print(choice)
                print("")

                # 移動方向を変換
                send_data = "{} {}".format(int(choice.real), int(choice.imag))
                # 移動方向をcomputer.pyに送信
                conn.sendall(send_data.encode())
