#--- Example ---
import numpy as np
import cv2 as cv # opencvのインポート
print('opencv version :',cv.__version__)

from matplotlib import pyplot as plt

#画像をグレスケで読み込む(引数で設定できる)
img = cv.imread('marker.png',cv.IMREAD_GRAYSCALE)

'''
plt.imshow(img, cmap = 'gray',)
plt.xticks([]), plt.yticks([]) # 軸の刻みを隠す
plt.show() #画像の表示
'''

print(img.shape) #次元(行、列、チャネル)
print(img.size) #総サイズ
print(img.dtype) #データ型

#flags = [i for i in dir(cv2) if i.startswith('COLOR_')]
#print (flags)

#要調整パラメタ
#円を円として認識してほしい
edges = cv.Canny(img,30,50)


ret,edges2 = cv.threshold(edges,127,255,cv.THRESH_BINARY)


plt.imshow(edges)
plt.title('Image'), plt.xticks([]), plt.yticks([])
plt.show()


img_temp = edges
contours, hierarchy = cv.findContours(img_temp, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)

#cv.cvtColor(edges, cv.COLOR_GRAY2RGB)
img_cont = cv.drawContours(edges, contours, -1, (255,0,0), 5)
#色はおかしいがとりあえず輪郭描写はできている、なぜか255,0,0にしないと不可


plt.imshow(img_cont)
plt.title('Image'), plt.xticks([]), plt.yticks([])
plt.show()


#Canny法のパラメタを調整しないとEgdeがバラバラになってしまう
#それだと重心が計算できない？？
cnt = contours[0]
M = cv.moments(cnt)
print (M)
